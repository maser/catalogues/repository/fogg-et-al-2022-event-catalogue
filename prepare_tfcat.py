#!/usr/bin/env python3
# -*- encode: utf-8 -*-

from pathlib import Path
import csv
from astropy.time import Time
from astropy.units import Unit
from tfcat import Feature, Polygon, CRS, FeatureCollection, dump
from shapely.geometry import shape
from shapely.ops import unary_union
import numpy

from load_raw_data import RAW_DATA_URL
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0.json"


def reader(csv_file: Path or str, version: str) -> dict:
    """
    Reads and yields raw data from the original CSV file
    :param csv_file: input file
    :param version: version of the dataset
    :return: row dictionary
    """

    # open up the CSV file
    with open(csv_file) as f:
        rows = csv.DictReader(f, delimiter=',', quotechar='"')
        seq = 0
        for row in rows:
            data = {
                "seq": seq,
                "start_time": row['stime'],
                "end_time": row['etime'],
                "burst_timestamp": [
                    Time(item.strip())
                    for item in row['burst_timestamp'].split(',')
                    if item.strip() != 'NaT'
                ],
                "min_f_bound": [
                    float(item.strip())*Unit("kHz")
                    for item in row['min_f_bound'].split(',')
                    if item.strip() != 'nan'
                ],
                "max_f_bound": [
                    float(item.strip())*Unit("kHz")
                    for item in row['max_f_bound'].split(',')
                    if item.strip() != 'nan'
                ],
                "properties": None
            }
            if version == "1.1":
                data["properties"] = dict([
                    (field_key, [
                        float(item.strip())
                        for item in row[field_key].split(',')
                        if item.strip() != 'nan'
                    ] * Unit(field_unit))
                    for field_key, field_unit
                    in zip(
                        ["LT_gse", "radius", "lat_gse", "lon_gse", "x_gse", "y_gse", "z_gse"],
                        ["h", "earthRad", "deg", "deg", "earthRad", "earthRad", "earthRad"]
                    )
                ])
            yield data
            seq += 1


def event_converter(data_source):
    """
    Converts the input data into a TFCat Feature

    :param data_source: iterator yielding row dictionaries (see reader function)
    :return: Feature
    """

    for input_data in data_source:
        tt = [item.unix for item in input_data['burst_timestamp']]
        fmin = [item.value for item in input_data['min_f_bound']]
        fmax = [item.value for item in input_data['max_f_bound']]
        contour = [list(zip(tt, fmin)) + list(zip(tt, fmax))[::-1] + [(tt[0], fmin[0])]]
        if input_data['properties'] is not None:
            properties = {}
            for field_key in input_data['properties'].keys():
                properties[field_key] = numpy.mean(input_data['properties'][field_key].value)
        else:
            properties = None
        yield Feature(
            id=input_data['seq'],
            geometry=Polygon(contour),
            properties=properties,
        )


def converter(input_file: Path, doi: str, version: str):
    """
    Converts the input data into a TFCat Feature Collection

    :param input_file: file name (CSV) to read the data from
    :param doi: DOI of the dataset
    :param version: version of the dataset
    :return: tuple with a dictionary with FeatureCollection values, a year list and a file list
    """

    print(f"Processing file: {input_file.name}")
    # define the global properties
    properties = {
        "instrument_host_name": "wind",
        "instrument_name": "waves",
        "receiver_name": "rad1",
        "target_name": "Earth",
        "target_class": "planet",
        "target_region": "magnetosphere",
        "feature_name": "Radio emissions#AKR#Auroral Kilometric Radiation",
        "title": "Bursts of Auroral Kilometric Radiation individually selected from Wind/WAVES data",
        "authors": [{
            "GivenName": "Alexandra",
            "FamilyName": "Fogg",
            "ORCID": "https://orcid.org/0000-0002-1139-5920",
            "Affiliation": "https://ror.org/051sx6d27"
        }, {
            "GivenName": "Caitriona",
            "FamilyName": "Jackman",
            "ORCID": "https://orcid.org/0000-0003-0635-7361",
            "Affiliation": "https://ror.org/051sx6d27"
        }, {
            "GivenName": "James E.",
            "FamilyName": "Waters",
            "ORCID": "https://orcid.org/0000-0001-8164-5414",
            "Affiliation": ["https://ror.org/01ryk1543", "https://ror.org/00ssy9q55"]
        }, {
            "GivenName": "Xavier",
            "FamilyName": "Bonnin",
            "ORCID": "https://orcid.org/0000-0003-4217-7333",
            "Affiliation": "https://ror.org/02eptjh02"
        }, {
            "GivenName": "Laurent",
            "FamilyName": "Lamy",
            "ORCID": "https://orcid.org/0000-0002-8428-1369",
            "Affiliation": ["https://ror.org/02eptjh02", "https://ror.org/00ssy9q55"]
        }, {
            "GivenName": "Baptiste",
            "FamilyName": "Cecconi",
            "ORCID": "https://orcid.org/0000-0001-7915-5571",
            "Affiliation": "https://ror.org/02eptjh02"
        }, {
            "GivenName": "Karine",
            "FamilyName": "Issautier",
            "ORCID": "https://orcid.org/0000-0002-2757-101X",
            "Affiliation": "https://ror.org/02eptjh02"
        }, {
            "GivenName": "Corentin K.",
            "FamilyName": "Louis",
            "ORCID": "https://orcid.org/0000-0002-9552-8822",
            "Affiliation": "https://ror.org/02eptjh02"
        }],
        "bib_reference": "https://doi.org/10.1029/2021JA030209",
        "doi": f"https://doi.org/{doi}",
        "publisher": "PADC",
        "version": version,
    }

    # define fields
    fields = {
        "1.0": None,
        "1.1": {
            "LT_gse": {
                "info": "Average value of Local Time in GSE coordinates",
                "datatype": "float",
                "ucd": "pos.bodyrcd.lon;pos.earth;stat.mean",
                "unit": "h",
            },
            "radius": {
                "info": "Average value of Radius in GSE coordinates",
                "datatype": "float",
                "ucd": "pos.distance;pos.earth;stat.mean",
                "unit": "Re",
            },
            "lat_gse": {
                "info": "Average value of Latitude in GSE coordinates",
                "datatype": "float",
                "ucd": "pos.bodyrc.lat;pos.earth;stat.mean",
                "unit": "deg",
            },
            "lon_gse": {
                "info": "Average value of Longiutude in GSE coordinates",
                "datatype": "float",
                "ucd": "pos.bodyrc.lon;pos.earth;stat.mean",
                "unit": "deg",
            },
            "x_gse": {
                "info": "Average value of X in GSE coordinates",
                "datatype": "float",
                "ucd": "pos.cartesian.x;pos.earth;stat.mean",
                "unit": "Re",
            },
            "y_gse": {
                "info": "Average value of Y in GSE coordinates",
                "datatype": "float",
                "ucd": "pos.cartesian.y;pos.earth;stat.mean",
                "unit": "Re",
            },
            "z_gse": {
                "info": "Average value of Z in GSE coordinates",
                "datatype": "float",
                "ucd": "pos.cartesian.z;pos.earth;stat.mean",
                "unit": "Re",
            },
        }
    }

    # define Coordinate Reference System
    crs = CRS({
        "type": "local",
        "properties": {
            "name": "Time-Frequency",
            "time_coords_id": "unix",
            "spectral_coords": {
                "type": "frequency",
                "unit": "kHz"
            },
            "ref_position_id": "wind",
        }
    })

    # Fill in a FeatureCollection object
    features = {}
    for event in event_converter(reader(input_file, version)):
        cur_year = Time(event.tmin, format='unix').to_datetime().year
        if cur_year not in features.keys():
            features[cur_year] = []
        features[cur_year].append(event)

    collections = {}
    years = []
    files = []

    for year in features.keys():
        bbox = unary_union([shape(event.geometry) for event in features[year]]).bounds
        properties['time_min'] = crs.time_converter(bbox[0]).isot
        properties['spectral_range_min'] = crs.spectral_converter(bbox[1]).to('Hz').value
        properties['time_max'] = crs.time_converter(bbox[2]).isot
        properties['spectral_range_max'] = crs.spectral_converter(bbox[3]).to('Hz').value
        collections[year] = FeatureCollection(
            schema=SCHEMA_URI,
            features=features[year],
            properties=properties,
            fields=fields[version],
            crs=crs
        )
        years.append(year)
        files.append(
            Path(__file__).parent / 'build' / f'{doi}' / 'content' / f'fogg_akr_burst_list_{year}.json'
        )

    return collections, years, files


def writer(input_file, doi, version, yearly=True):
    """writes out a TFCat JSON file

    :param input_file: Path to the input file
    :param doi: DOI of the dataset
    :param version: version of the dataset
    """

    collections, years, files = converter(input_file, doi, version)

    transfer_batch = []
    for cur_file, year in zip(files, years):
        print(f"Writing file: {cur_file.name}")
        cur_file.parent.mkdir(parents=True, exist_ok=True)
        with open(cur_file, 'w') as f:
            dump(collections[year], f)
        print(f"Validating file: {cur_file.name}")
        validate(cur_file)
        transfer_batch.append(f"scp {cur_file} vmmaser_rpws:/volumes/kronos/doi/{doi}/content")

    print("\nscp commands:\n-------------\n")
    for line in transfer_batch:
        print(line)
    print()


def validate(file_name):
    from tfcat.validate import validate_file
    validate_file(file_name, schema_uri="https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0.json")


if __name__ == "__main__":
    for version in RAW_DATA_URL.keys():
        print(f"Processing version: {version}")
        for input_data in RAW_DATA_URL[version]:
            input_file = Path(__file__).parent / "data" / input_data['url'].split("/")[-1]
            writer(input_file, input_data['doi'], version)
