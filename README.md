# Fogg et al 2022 - Event Catalogue

This script has been used to produce the supplementary material TFCat version 
of Fogg et al. (2022).

The TFCat file is prepared from a CSV file containing bounding boxes of AKR 
(Auroral Kilometric Radiation) bursts measured by the Wind/Waves receiver.

## Run the example

```bash
python load_raw_data.py
python prepare_tfcat.py
```

## References

- Fogg, A. R., Jackman, C. M., Waters, J. E., Bonnin, X., Lamy, L., Cecconi,  
  B., et al. (**2022**). *Wind/WAVES Observations of Auroral Kilometric Radiation:
  Automated Burst Detection and Terrestrial Solar Wind - Magnetosphere Coupling 
  Effects*. J. Geophys. Res. Space Phys., 127(5), e2021JA030209. 
  [doi:10.1029/2021ja030209](https://doi.org/10.1029/2021ja030209)
