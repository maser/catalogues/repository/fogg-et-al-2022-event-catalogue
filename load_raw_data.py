#!/usr/bin/env python3
# -*- encode: utf-8 -*-
import json
import requests
from pathlib import Path


def load_metadata_file(filename="datasets.json") -> dict:
    with open(filename, "r") as metadata_file:
        return json.load(metadata_file)


RAW_DATA_URL = load_metadata_file()


def download_raw_data(version='1.0'):
    """Download the raw data from https://maser.obspm.fr/doi
    :param version: The version of the data to be downloaded (defaults to '1.0')
    """
    print(f"Downloading v{version}")
    for data in RAW_DATA_URL[version]:
        r = requests.get(data['url'], allow_redirects=True)
        if r.status_code == 200:
            file_path = Path(__file__).parent / "data" / data['url'].split('/')[-1]
            with open(file_path, 'wb') as f:
                f.write(r.content)
                print(f"Downloaded: {data['url']}")


if __name__ == '__main__':
    download_raw_data('1.0')
    download_raw_data('1.1')
    print("Done!")
